﻿using System;
using System.Collections.Generic;

namespace app.model
{
    public partial class AdmissionDetails
    {
        public AdmissionDetails()
        {
            FeeDetails = new HashSet<FeeDetails>();
            TrainerBatchDetails = new HashSet<TrainerBatchDetails>();
        }

        public Guid StudentId { get; set; }
        public Guid AdmissionId { get; set; }
        public string CreatedDate { get; set; }
        public Guid? CourseId { get; set; }
        public bool? IsActive { get; set; }
        public string RegCode { get; set; }
        public Guid? PreviousCourseId { get; set; }

        public StudentDetails Student { get; set; }
        public ICollection<FeeDetails> FeeDetails { get; set; }
        public ICollection<TrainerBatchDetails> TrainerBatchDetails { get; set; }
    }
}
