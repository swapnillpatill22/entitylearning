﻿using System;
using System.Collections.Generic;

namespace app.model
{
    public partial class EmployeeDetails
    {
        public EmployeeDetails()
        {
            TrainerPaymentDetails = new HashSet<TrainerPaymentDetails>();
        }

        public Guid EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string MoblileNumber { get; set; }
        public string EmailId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Gender { get; set; }
        public Guid? EmpRoleId { get; set; }
        public Guid? BranchId { get; set; }
        public decimal? TrainerPercent { get; set; }
        public bool? IsActive { get; set; }

        public BranchDetails Branch { get; set; }
        public EmployeeRoles EmpRole { get; set; }
        public ICollection<TrainerPaymentDetails> TrainerPaymentDetails { get; set; }
    }
}
