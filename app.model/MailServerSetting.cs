﻿using System;
using System.Collections.Generic;

namespace app.model
{
    public partial class MailServerSetting
    {
        public MailServerSetting()
        {
            BranchMailServerSettings = new HashSet<BranchMailServerSettings>();
        }

        public string CredentialsUserName { get; set; }
        public string CredetialsPassword { get; set; }
        public string Host { get; set; }
        public string HostPortNo { get; set; }
        public string PopClient { get; set; }
        public string PopClientPortNumber { get; set; }
        public string ServerName { get; set; }
        public Guid MailServerSettingId { get; set; }
        public bool? EnableSsl { get; set; }
        public bool? IsActive { get; set; }

        public ICollection<BranchMailServerSettings> BranchMailServerSettings { get; set; }
    }
}
