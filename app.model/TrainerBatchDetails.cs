﻿using System;
using System.Collections.Generic;

namespace app.model
{
    public partial class TrainerBatchDetails
    {
        public Guid Id { get; set; }
        public Guid? TrainerId { get; set; }
        public Guid? AdmissionId { get; set; }

        public AdmissionDetails Admission { get; set; }
        public TrainerPaymentDetails Trainer { get; set; }
    }
}
