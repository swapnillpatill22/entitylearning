﻿using System;
using System.Collections.Generic;

namespace app.model
{
    public partial class TrainerVoucher
    {
        public Guid VoucherId { get; set; }
        public string Name { get; set; }
        public int? VoucherNo { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public decimal? Amount { get; set; }
        public string Note { get; set; }
        public DateTime? Date { get; set; }
        public string Time { get; set; }
        public Guid? FinancialYearId { get; set; }
        public string PaymentMode { get; set; }
        public string User { get; set; }
        public bool? IsActive { get; set; }
        public Guid? EmployeeId { get; set; }
    }
}
