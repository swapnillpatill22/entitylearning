﻿using System;
using System.Collections.Generic;

namespace app.model
{
    public partial class FinicialYear
    {
        public FinicialYear()
        {
            CourseDetails = new HashSet<CourseDetails>();
            EnquiryDeatils = new HashSet<EnquiryDeatils>();
            StudentDetails = new HashSet<StudentDetails>();
            TrainerPaymentDetails = new HashSet<TrainerPaymentDetails>();
        }

        public Guid FinancialYearId { get; set; }
        public string FinanicialYear { get; set; }
        public Guid? BranchId { get; set; }

        public BranchDetails Branch { get; set; }
        public ICollection<CourseDetails> CourseDetails { get; set; }
        public ICollection<EnquiryDeatils> EnquiryDeatils { get; set; }
        public ICollection<StudentDetails> StudentDetails { get; set; }
        public ICollection<TrainerPaymentDetails> TrainerPaymentDetails { get; set; }
    }
}
