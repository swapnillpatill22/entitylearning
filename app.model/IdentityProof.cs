﻿using System;
using System.Collections.Generic;

namespace app.model
{
    public partial class IdentityProof
    {
        public Guid IdentityProofId { get; set; }
        public string Title { get; set; }
    }
}
