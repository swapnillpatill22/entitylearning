﻿using System;
using System.Collections.Generic;

namespace app.model
{
    public partial class PackageDetails
    {
        public Guid CourseId { get; set; }
        public Guid PackageId { get; set; }
        public bool? IsActive { get; set; }
        public int Id { get; set; }
        public decimal? Percent { get; set; }
        public decimal? Amount { get; set; }
    }
}
