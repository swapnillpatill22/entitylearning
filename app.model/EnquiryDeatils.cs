﻿using System;
using System.Collections.Generic;

namespace app.model
{
    public partial class EnquiryDeatils
    {
        public Guid EnquiryId { get; set; }
        public int? SrNo { get; set; }
        public Guid? StudentId { get; set; }
        public Guid? CouncelerId { get; set; }
        public string CreateDate { get; set; }
        public string Cource1 { get; set; }
        public string Cource2 { get; set; }
        public string Cource3 { get; set; }
        public string Cource4 { get; set; }
        public string Sources { get; set; }
        public string Refrence { get; set; }
        public Guid? FinancialYearId { get; set; }
        public bool? IsActive { get; set; }

        public FinicialYear FinancialYear { get; set; }
        public StudentDetails Student { get; set; }
    }
}
