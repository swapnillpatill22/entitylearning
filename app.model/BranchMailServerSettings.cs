﻿using System;
using System.Collections.Generic;

namespace app.model
{
    public partial class BranchMailServerSettings
    {
        public Guid? MailServerSettingId { get; set; }
        public Guid BranchId { get; set; }
        public bool? IsActive { get; set; }

        public BranchDetails Branch { get; set; }
        public MailServerSetting MailServerSetting { get; set; }
    }
}
