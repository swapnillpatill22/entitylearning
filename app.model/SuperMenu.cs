﻿using System;
using System.Collections.Generic;

namespace app.model
{
    public partial class SuperMenu
    {
        public SuperMenu()
        {
            SubMenuMaster = new HashSet<SubMenuMaster>();
        }

        public int MainMenuId { get; set; }
        public string MenuName { get; set; }
        public string Icon { get; set; }

        public ICollection<SubMenuMaster> SubMenuMaster { get; set; }
    }
}
