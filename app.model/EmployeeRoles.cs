﻿using System;
using System.Collections.Generic;

namespace app.model
{
    public partial class EmployeeRoles
    {
        public EmployeeRoles()
        {
            EmployeeDetails = new HashSet<EmployeeDetails>();
        }

        public Guid EmpRoleId { get; set; }
        public string Roles { get; set; }

        public ICollection<EmployeeDetails> EmployeeDetails { get; set; }
    }
}
