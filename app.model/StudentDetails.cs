﻿using System;
using System.Collections.Generic;

namespace app.model
{
    public partial class StudentDetails
    {
        public StudentDetails()
        {
            AdmissionDetails = new HashSet<AdmissionDetails>();
            EnquiryDeatils = new HashSet<EnquiryDeatils>();
        }

        public Guid StudentId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Qualification { get; set; }
        public string College { get; set; }
        public string LandlineNo { get; set; }
        public string Mobile1 { get; set; }
        public string Mobile2 { get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public Guid? FinancialYearId { get; set; }
        public bool? IsActive { get; set; }
        public string DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string ParentName { get; set; }
        public string Relation { get; set; }
        public string Organisation { get; set; }
        public string Occupation { get; set; }
        public string MobileNo1 { get; set; }
        public string MobileNo2 { get; set; }
        public string Email { get; set; }

        public FinicialYear FinancialYear { get; set; }
        public ICollection<AdmissionDetails> AdmissionDetails { get; set; }
        public ICollection<EnquiryDeatils> EnquiryDeatils { get; set; }
    }
}
