﻿using System;
using System.Collections.Generic;

namespace app.model
{
    public partial class BankDetails
    {
        public Guid BankId { get; set; }
        public string BankName { get; set; }
        public string Ifsccode { get; set; }
        public bool? IsActive { get; set; }
    }
}
