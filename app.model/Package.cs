﻿using System;
using System.Collections.Generic;

namespace app.model
{
    public partial class Package
    {
        public Guid PackageId { get; set; }
        public string Title { get; set; }
        public string CreatedDate { get; set; }
        public bool? IsActive { get; set; }
        public Guid? FinancialYearId { get; set; }
        public decimal? Amount { get; set; }
    }
}
