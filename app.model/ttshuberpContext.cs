﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace app.model
{
    public partial class ttshuberpContext : DbContext
    {
        public virtual DbSet<AdddressProof> AdddressProof { get; set; }
        public virtual DbSet<AdmissionDetails> AdmissionDetails { get; set; }
        public virtual DbSet<AspnetApplications> AspnetApplications { get; set; }
        public virtual DbSet<AspnetMembership> AspnetMembership { get; set; }
        public virtual DbSet<AspnetPaths> AspnetPaths { get; set; }
        public virtual DbSet<AspnetPersonalizationAllUsers> AspnetPersonalizationAllUsers { get; set; }
        public virtual DbSet<AspnetPersonalizationPerUser> AspnetPersonalizationPerUser { get; set; }
        public virtual DbSet<AspnetProfile> AspnetProfile { get; set; }
        public virtual DbSet<AspnetRoles> AspnetRoles { get; set; }
        public virtual DbSet<AspnetSchemaVersions> AspnetSchemaVersions { get; set; }
        public virtual DbSet<AspnetUsers> AspnetUsers { get; set; }
        public virtual DbSet<AspnetUsersInRoles> AspnetUsersInRoles { get; set; }
        public virtual DbSet<AspnetWebEventEvents> AspnetWebEventEvents { get; set; }
        public virtual DbSet<BankDetails> BankDetails { get; set; }
        public virtual DbSet<BranchDetails> BranchDetails { get; set; }
        public virtual DbSet<BranchMailServerSettings> BranchMailServerSettings { get; set; }
        public virtual DbSet<CourseDetails> CourseDetails { get; set; }
        public virtual DbSet<EmployeeDetails> EmployeeDetails { get; set; }
        public virtual DbSet<EmployeeRoles> EmployeeRoles { get; set; }
        public virtual DbSet<EnquiryDeatils> EnquiryDeatils { get; set; }
        public virtual DbSet<ErrorLog> ErrorLog { get; set; }
        public virtual DbSet<FeeDetails> FeeDetails { get; set; }
        public virtual DbSet<FeeReceiptHistory> FeeReceiptHistory { get; set; }
        public virtual DbSet<FinicialYear> FinicialYear { get; set; }
        public virtual DbSet<IdentityProof> IdentityProof { get; set; }
        public virtual DbSet<IssueCashDetails> IssueCashDetails { get; set; }
        public virtual DbSet<IssueVoucher> IssueVoucher { get; set; }
        public virtual DbSet<MailServerSetting> MailServerSetting { get; set; }
        public virtual DbSet<Package> Package { get; set; }
        public virtual DbSet<PackageDetails> PackageDetails { get; set; }
        public virtual DbSet<StudentDetails> StudentDetails { get; set; }
        public virtual DbSet<SubMenuMaster> SubMenuMaster { get; set; }
        public virtual DbSet<SuperMenu> SuperMenu { get; set; }
        public virtual DbSet<TrainerBatchDetails> TrainerBatchDetails { get; set; }
        public virtual DbSet<TrainerPaymentDetails> TrainerPaymentDetails { get; set; }
        public virtual DbSet<TrainerVoucher> TrainerVoucher { get; set; }

        // Unable to generate entity type for table 'dbo.AdmissionCourses'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.AccountTransactions'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Issue_Voucher_OnlineDetails'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Issue_Vouchr_ChequeDetails'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Receive_Voucher'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Receive_Voucher_ChequeDetails'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Receive_Voucher_OnlineDetails'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Trainer_Voucher_ChequeDetails'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Trainer_Voucher_OnlineDetails'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.BranchMenuRestrictions'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.BulkEmail'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ChequePaymentDetails'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.DocumentSubmitted'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.EmployeeMenuRestriction'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.EmployeeReminders'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Email_Failed'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.FollowUp'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.FollowUpConversation'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.NewsSources'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.PreviousAdmission'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.StudentImages'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.AccountTransactionHIstory'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.TenantDetails'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.TrainerTotalPaymentDetails'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Trainer_Cources'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.TrainerPaymentHistory'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Fees_History'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(@"data source=192.168.0.121,50667;initial catalog=ttshuberp;User Id=swapnil; password=swapnil@#");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AdddressProof>(entity =>
            {
                entity.HasKey(e => e.AddressProofId);

                entity.Property(e => e.AddressProofId).ValueGeneratedNever();

                entity.Property(e => e.Title).HasMaxLength(50);
            });

            modelBuilder.Entity<AdmissionDetails>(entity =>
            {
                entity.HasKey(e => e.AdmissionId);

                entity.Property(e => e.AdmissionId).ValueGeneratedNever();

                entity.Property(e => e.CreatedDate).HasMaxLength(50);

                entity.Property(e => e.RegCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.AdmissionDetails)
                    .HasForeignKey(d => d.StudentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AdmissionDetails_StudentDetails");
            });

            modelBuilder.Entity<AspnetApplications>(entity =>
            {
                entity.HasKey(e => e.ApplicationId)
                    .ForSqlServerIsClustered(false);

                entity.ToTable("aspnet_Applications");

                entity.HasIndex(e => e.ApplicationName)
                    .HasName("UQ__aspnet_A__309103310936BE45")
                    .IsUnique();

                entity.HasIndex(e => e.LoweredApplicationName)
                    .HasName("UQ__aspnet_A__17477DE4997371CD")
                    .IsUnique();

                entity.Property(e => e.ApplicationId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ApplicationName)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.Description).HasMaxLength(256);

                entity.Property(e => e.LoweredApplicationName)
                    .IsRequired()
                    .HasMaxLength(256);
            });

            modelBuilder.Entity<AspnetMembership>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .ForSqlServerIsClustered(false);

                entity.ToTable("aspnet_Membership");

                entity.Property(e => e.UserId).ValueGeneratedNever();

                entity.Property(e => e.Comment).HasColumnType("ntext");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.FailedPasswordAnswerAttemptWindowStart).HasColumnType("datetime");

                entity.Property(e => e.FailedPasswordAttemptWindowStart).HasColumnType("datetime");

                entity.Property(e => e.LastLockoutDate).HasColumnType("datetime");

                entity.Property(e => e.LastLoginDate).HasColumnType("datetime");

                entity.Property(e => e.LastPasswordChangedDate).HasColumnType("datetime");

                entity.Property(e => e.LoweredEmail).HasMaxLength(256);

                entity.Property(e => e.MobilePin)
                    .HasColumnName("MobilePIN")
                    .HasMaxLength(16);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.PasswordAnswer).HasMaxLength(128);

                entity.Property(e => e.PasswordFormat).HasDefaultValueSql("((0))");

                entity.Property(e => e.PasswordQuestion).HasMaxLength(256);

                entity.Property(e => e.PasswordSalt)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.HasOne(d => d.Application)
                    .WithMany(p => p.AspnetMembership)
                    .HasForeignKey(d => d.ApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__aspnet_Me__Appli__5FB337D6");

                entity.HasOne(d => d.User)
                    .WithOne(p => p.AspnetMembership)
                    .HasForeignKey<AspnetMembership>(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__aspnet_Me__UserI__60A75C0F");
            });

            modelBuilder.Entity<AspnetPaths>(entity =>
            {
                entity.HasKey(e => e.PathId)
                    .ForSqlServerIsClustered(false);

                entity.ToTable("aspnet_Paths");

                entity.Property(e => e.PathId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.LoweredPath)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.Path)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.HasOne(d => d.Application)
                    .WithMany(p => p.AspnetPaths)
                    .HasForeignKey(d => d.ApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__aspnet_Pa__Appli__619B8048");
            });

            modelBuilder.Entity<AspnetPersonalizationAllUsers>(entity =>
            {
                entity.HasKey(e => e.PathId);

                entity.ToTable("aspnet_PersonalizationAllUsers");

                entity.Property(e => e.PathId).ValueGeneratedNever();

                entity.Property(e => e.LastUpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.PageSettings)
                    .IsRequired()
                    .HasColumnType("image");

                entity.HasOne(d => d.Path)
                    .WithOne(p => p.AspnetPersonalizationAllUsers)
                    .HasForeignKey<AspnetPersonalizationAllUsers>(d => d.PathId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__aspnet_Pe__PathI__628FA481");
            });

            modelBuilder.Entity<AspnetPersonalizationPerUser>(entity =>
            {
                entity.ToTable("aspnet_PersonalizationPerUser");

                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.LastUpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.PageSettings)
                    .IsRequired()
                    .HasColumnType("image");

                entity.HasOne(d => d.Path)
                    .WithMany(p => p.AspnetPersonalizationPerUser)
                    .HasForeignKey(d => d.PathId)
                    .HasConstraintName("FK__aspnet_Pe__PathI__6383C8BA");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspnetPersonalizationPerUser)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK__aspnet_Pe__UserI__6477ECF3");
            });

            modelBuilder.Entity<AspnetProfile>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("aspnet_Profile");

                entity.Property(e => e.UserId).ValueGeneratedNever();

                entity.Property(e => e.LastUpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.PropertyNames)
                    .IsRequired()
                    .HasColumnType("ntext");

                entity.Property(e => e.PropertyValuesBinary)
                    .IsRequired()
                    .HasColumnType("image");

                entity.Property(e => e.PropertyValuesString)
                    .IsRequired()
                    .HasColumnType("ntext");

                entity.HasOne(d => d.User)
                    .WithOne(p => p.AspnetProfile)
                    .HasForeignKey<AspnetProfile>(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__aspnet_Pr__UserI__656C112C");
            });

            modelBuilder.Entity<AspnetRoles>(entity =>
            {
                entity.HasKey(e => e.RoleId)
                    .ForSqlServerIsClustered(false);

                entity.ToTable("aspnet_Roles");

                entity.Property(e => e.RoleId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Description).HasMaxLength(256);

                entity.Property(e => e.LoweredRoleName)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.HasOne(d => d.Application)
                    .WithMany(p => p.AspnetRoles)
                    .HasForeignKey(d => d.ApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__aspnet_Ro__Appli__66603565");
            });

            modelBuilder.Entity<AspnetSchemaVersions>(entity =>
            {
                entity.HasKey(e => new { e.Feature, e.CompatibleSchemaVersion });

                entity.ToTable("aspnet_SchemaVersions");

                entity.Property(e => e.Feature).HasMaxLength(128);

                entity.Property(e => e.CompatibleSchemaVersion).HasMaxLength(128);
            });

            modelBuilder.Entity<AspnetUsers>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .ForSqlServerIsClustered(false);

                entity.ToTable("aspnet_Users");

                entity.Property(e => e.UserId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.LastActivityDate).HasColumnType("datetime");

                entity.Property(e => e.LoweredUserName)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.MobileAlias).HasMaxLength(16);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.HasOne(d => d.Application)
                    .WithMany(p => p.AspnetUsers)
                    .HasForeignKey(d => d.ApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__aspnet_Us__Appli__6754599E");
            });

            modelBuilder.Entity<AspnetUsersInRoles>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId });

                entity.ToTable("aspnet_UsersInRoles");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspnetUsersInRoles)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__aspnet_Us__RoleI__68487DD7");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspnetUsersInRoles)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__aspnet_Us__UserI__693CA210");
            });

            modelBuilder.Entity<AspnetWebEventEvents>(entity =>
            {
                entity.HasKey(e => e.EventId);

                entity.ToTable("aspnet_WebEvent_Events");

                entity.Property(e => e.EventId)
                    .HasColumnType("char(32)")
                    .ValueGeneratedNever();

                entity.Property(e => e.ApplicationPath).HasMaxLength(256);

                entity.Property(e => e.ApplicationVirtualPath).HasMaxLength(256);

                entity.Property(e => e.Details).HasColumnType("ntext");

                entity.Property(e => e.EventOccurrence).HasColumnType("decimal(19, 0)");

                entity.Property(e => e.EventSequence).HasColumnType("decimal(19, 0)");

                entity.Property(e => e.EventTime).HasColumnType("datetime");

                entity.Property(e => e.EventTimeUtc).HasColumnType("datetime");

                entity.Property(e => e.EventType)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.ExceptionType).HasMaxLength(256);

                entity.Property(e => e.MachineName)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.Message).HasMaxLength(1024);

                entity.Property(e => e.RequestUrl).HasMaxLength(1024);
            });

            modelBuilder.Entity<BankDetails>(entity =>
            {
                entity.HasKey(e => e.BankId);

                entity.ToTable("Bank_Details");

                entity.Property(e => e.BankId).ValueGeneratedNever();

                entity.Property(e => e.BankName).HasMaxLength(50);

                entity.Property(e => e.Ifsccode)
                    .HasColumnName("IFSCCode")
                    .HasMaxLength(50);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<BranchDetails>(entity =>
            {
                entity.HasKey(e => e.BranchId);

                entity.Property(e => e.BranchId).ValueGeneratedNever();

                entity.Property(e => e.Address)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.BranchName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BranchPrefix)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasMaxLength(30);

                entity.Property(e => e.PinCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.State)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<BranchMailServerSettings>(entity =>
            {
                entity.HasKey(e => e.BranchId);

                entity.Property(e => e.BranchId).ValueGeneratedNever();

                entity.HasOne(d => d.Branch)
                    .WithOne(p => p.BranchMailServerSettings)
                    .HasForeignKey<BranchMailServerSettings>(d => d.BranchId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BranchMailServerSettings_BranchDetails");

                entity.HasOne(d => d.MailServerSetting)
                    .WithMany(p => p.BranchMailServerSettings)
                    .HasForeignKey(d => d.MailServerSettingId)
                    .HasConstraintName("FK_BranchMailServerSettings_MailServerSetting");
            });

            modelBuilder.Entity<CourseDetails>(entity =>
            {
                entity.HasKey(e => e.CourseId);

                entity.Property(e => e.CourseId).ValueGeneratedNever();

                entity.Property(e => e.CourseName).HasMaxLength(250);

                entity.Property(e => e.CreatedDate).HasMaxLength(50);

                entity.HasOne(d => d.FinancialYear)
                    .WithMany(p => p.CourseDetails)
                    .HasForeignKey(d => d.FinancialYearId)
                    .HasConstraintName("FK_CourseDetails_FinicialYear");
            });

            modelBuilder.Entity<EmployeeDetails>(entity =>
            {
                entity.HasKey(e => e.EmployeeId);

                entity.Property(e => e.EmployeeId)
                    .HasColumnName("EmployeeID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Address)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmailId).HasMaxLength(256);

                entity.Property(e => e.EmployeeName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gender)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.MoblileNumber).HasMaxLength(40);

                entity.Property(e => e.PhoneNumber).HasMaxLength(40);

                entity.Property(e => e.TrainerPercent).HasColumnType("decimal(10, 2)");

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.EmployeeDetails)
                    .HasForeignKey(d => d.BranchId)
                    .HasConstraintName("FK_EmployeeDetails_BranchDetails");

                entity.HasOne(d => d.EmpRole)
                    .WithMany(p => p.EmployeeDetails)
                    .HasForeignKey(d => d.EmpRoleId)
                    .HasConstraintName("FK_EmployeeDetails_EmployeeRoles");
            });

            modelBuilder.Entity<EmployeeRoles>(entity =>
            {
                entity.HasKey(e => e.EmpRoleId);

                entity.Property(e => e.EmpRoleId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Roles)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EnquiryDeatils>(entity =>
            {
                entity.HasKey(e => e.EnquiryId);

                entity.Property(e => e.EnquiryId).ValueGeneratedNever();

                entity.Property(e => e.Cource1).HasMaxLength(50);

                entity.Property(e => e.Cource2).HasMaxLength(50);

                entity.Property(e => e.Cource3).HasMaxLength(50);

                entity.Property(e => e.Cource4).HasMaxLength(50);

                entity.Property(e => e.CreateDate).HasMaxLength(50);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.Refrence).HasMaxLength(50);

                entity.Property(e => e.Sources).HasMaxLength(50);

                entity.HasOne(d => d.FinancialYear)
                    .WithMany(p => p.EnquiryDeatils)
                    .HasForeignKey(d => d.FinancialYearId)
                    .HasConstraintName("FK_EnquiryDeatils_FinicialYear");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.EnquiryDeatils)
                    .HasForeignKey(d => d.StudentId)
                    .HasConstraintName("FK_EnquiryDeatils_StudentDetails");
            });

            modelBuilder.Entity<ErrorLog>(entity =>
            {
                entity.HasKey(e => e.ErrorId);

                entity.Property(e => e.DateTime).HasColumnType("date");

                entity.Property(e => e.MethodName).HasMaxLength(50);

                entity.Property(e => e.PageName).HasMaxLength(50);

                entity.Property(e => e.StackTrace).HasMaxLength(500);
            });

            modelBuilder.Entity<FeeDetails>(entity =>
            {
                entity.HasKey(e => e.FeeId);

                entity.Property(e => e.FeeId).ValueGeneratedNever();

                entity.Property(e => e.BankName).HasMaxLength(50);

                entity.Property(e => e.ChequeNo).HasMaxLength(50);

                entity.Property(e => e.Dated).HasMaxLength(50);

                entity.Property(e => e.DueDate).HasMaxLength(50);

                entity.Property(e => e.FeeAmount).HasColumnType("money");

                entity.Property(e => e.FeeDue).HasColumnType("money");

                entity.Property(e => e.NextDue).HasMaxLength(50);

                entity.Property(e => e.PaidFee).HasColumnType("money");

                entity.HasOne(d => d.Admission)
                    .WithMany(p => p.FeeDetails)
                    .HasForeignKey(d => d.AdmissionId)
                    .HasConstraintName("FK_FeeDetails_AdmissionDetails");
            });

            modelBuilder.Entity<FeeReceiptHistory>(entity =>
            {
                entity.HasKey(e => e.FeeReceiptId);

                entity.Property(e => e.FeeReceiptId).ValueGeneratedNever();

                entity.Property(e => e.BankName).HasMaxLength(50);

                entity.Property(e => e.ChequeNo).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasMaxLength(50);

                entity.Property(e => e.Dated).HasMaxLength(50);

                entity.Property(e => e.DueDate).HasMaxLength(50);

                entity.Property(e => e.FeeAmount).HasColumnType("money");

                entity.Property(e => e.FeeDue).HasColumnType("money");

                entity.Property(e => e.NextDue).HasMaxLength(50);

                entity.Property(e => e.PaidFee).HasColumnType("money");
            });

            modelBuilder.Entity<FinicialYear>(entity =>
            {
                entity.HasKey(e => e.FinancialYearId);

                entity.Property(e => e.FinancialYearId).ValueGeneratedNever();

                entity.Property(e => e.FinanicialYear)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.FinicialYear)
                    .HasForeignKey(d => d.BranchId)
                    .HasConstraintName("FK_FinicialYear_BranchDetails");
            });

            modelBuilder.Entity<IdentityProof>(entity =>
            {
                entity.Property(e => e.IdentityProofId)
                    .HasColumnName("identityProofId")
                    .ValueGeneratedNever();

                entity.Property(e => e.Title).HasMaxLength(50);
            });

            modelBuilder.Entity<IssueCashDetails>(entity =>
            {
                entity.HasKey(e => e.CashId);

                entity.Property(e => e.CashId).ValueGeneratedNever();

                entity.Property(e => e.Amount).HasColumnType("money");

                entity.Property(e => e.Date).HasMaxLength(50);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.Name).HasColumnType("nchar(50)");
            });

            modelBuilder.Entity<IssueVoucher>(entity =>
            {
                entity.HasKey(e => e.VoucherId);

                entity.ToTable("Issue_Voucher");

                entity.Property(e => e.VoucherId).ValueGeneratedNever();

                entity.Property(e => e.Amount).HasColumnType("money");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.MobileNo).HasMaxLength(20);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.PaymentMode).HasMaxLength(20);

                entity.Property(e => e.Time).HasMaxLength(20);

                entity.Property(e => e.User).HasMaxLength(50);
            });

            modelBuilder.Entity<MailServerSetting>(entity =>
            {
                entity.Property(e => e.MailServerSettingId).ValueGeneratedNever();

                entity.Property(e => e.CredentialsUserName).HasMaxLength(50);

                entity.Property(e => e.CredetialsPassword).HasMaxLength(50);

                entity.Property(e => e.Host)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HostPortNo)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.PopClient).HasMaxLength(50);

                entity.Property(e => e.PopClientPortNumber).HasMaxLength(50);

                entity.Property(e => e.ServerName).HasMaxLength(50);
            });

            modelBuilder.Entity<Package>(entity =>
            {
                entity.Property(e => e.PackageId).ValueGeneratedNever();

                entity.Property(e => e.Amount).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.CreatedDate).HasMaxLength(50);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.Title).HasMaxLength(50);
            });

            modelBuilder.Entity<PackageDetails>(entity =>
            {
                entity.Property(e => e.Amount).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.Percent).HasColumnType("decimal(10, 2)");
            });

            modelBuilder.Entity<StudentDetails>(entity =>
            {
                entity.HasKey(e => e.StudentId);

                entity.Property(e => e.StudentId).ValueGeneratedNever();

                entity.Property(e => e.Address).HasMaxLength(200);

                entity.Property(e => e.College).HasMaxLength(50);

                entity.Property(e => e.DateOfBirth).HasMaxLength(50);

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.Email1).HasMaxLength(500);

                entity.Property(e => e.Email2).HasMaxLength(500);

                entity.Property(e => e.FirstName).HasMaxLength(50);

                entity.Property(e => e.Gender).HasMaxLength(50);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.LandlineNo).HasMaxLength(15);

                entity.Property(e => e.LastName).HasMaxLength(50);

                entity.Property(e => e.MiddleName).HasMaxLength(50);

                entity.Property(e => e.Mobile1).HasMaxLength(50);

                entity.Property(e => e.Mobile2).HasMaxLength(50);

                entity.Property(e => e.MobileNo1).HasMaxLength(50);

                entity.Property(e => e.MobileNo2).HasMaxLength(50);

                entity.Property(e => e.Occupation).HasMaxLength(50);

                entity.Property(e => e.Organisation).HasMaxLength(50);

                entity.Property(e => e.ParentName).HasMaxLength(50);

                entity.Property(e => e.Qualification).HasMaxLength(50);

                entity.Property(e => e.Relation).HasMaxLength(50);

                entity.HasOne(d => d.FinancialYear)
                    .WithMany(p => p.StudentDetails)
                    .HasForeignKey(d => d.FinancialYearId)
                    .HasConstraintName("FK_StudentDetails_FinicialYear");
            });

            modelBuilder.Entity<SubMenuMaster>(entity =>
            {
                entity.HasKey(e => e.SubMenuId);

                entity.Property(e => e.SubMenuId).ValueGeneratedNever();

                entity.Property(e => e.SubMenuName).HasMaxLength(50);

                entity.Property(e => e.SubMenuUrl)
                    .HasColumnName("SubMenuURL")
                    .HasMaxLength(500);

                entity.HasOne(d => d.MainMenu)
                    .WithMany(p => p.SubMenuMaster)
                    .HasForeignKey(d => d.MainMenuId)
                    .HasConstraintName("FK_SubMenuMaster_SuperMenu");
            });

            modelBuilder.Entity<SuperMenu>(entity =>
            {
                entity.HasKey(e => e.MainMenuId);

                entity.Property(e => e.MainMenuId).ValueGeneratedNever();

                entity.Property(e => e.Icon).HasMaxLength(50);

                entity.Property(e => e.MenuName).HasMaxLength(50);
            });

            modelBuilder.Entity<TrainerBatchDetails>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.Admission)
                    .WithMany(p => p.TrainerBatchDetails)
                    .HasForeignKey(d => d.AdmissionId)
                    .HasConstraintName("FK_TrainerBatchDetails_AdmissionDetails");

                entity.HasOne(d => d.Trainer)
                    .WithMany(p => p.TrainerBatchDetails)
                    .HasForeignKey(d => d.TrainerId)
                    .HasConstraintName("FK_TrainerBatchDetails_TrainerPaymentDetails");
            });

            modelBuilder.Entity<TrainerPaymentDetails>(entity =>
            {
                entity.HasKey(e => e.TrainerId);

                entity.Property(e => e.TrainerId).ValueGeneratedNever();

                entity.Property(e => e.CreatedDate).HasMaxLength(50);

                entity.Property(e => e.LastVoucherCreated).HasMaxLength(50);

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.TrainerPaymentDetails)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK_TrainerPaymentDetails_EmployeeDetails");

                entity.HasOne(d => d.FinancialYear)
                    .WithMany(p => p.TrainerPaymentDetails)
                    .HasForeignKey(d => d.FinancialYearId)
                    .HasConstraintName("FK_TrainerPaymentDetails_FinicialYear");
            });

            modelBuilder.Entity<TrainerVoucher>(entity =>
            {
                entity.HasKey(e => e.VoucherId);

                entity.ToTable("Trainer_Voucher");

                entity.Property(e => e.VoucherId).ValueGeneratedNever();

                entity.Property(e => e.Amount).HasColumnType("money");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.MobileNo).HasMaxLength(20);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.PaymentMode).HasMaxLength(20);

                entity.Property(e => e.Time).HasMaxLength(20);

                entity.Property(e => e.User).HasMaxLength(50);
            });
        }
    }
}
