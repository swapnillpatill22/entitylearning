﻿using System;
using System.Collections.Generic;

namespace app.model
{
    public partial class SubMenuMaster
    {
        public int SubMenuId { get; set; }
        public string SubMenuName { get; set; }
        public int? MainMenuId { get; set; }
        public string SubMenuUrl { get; set; }

        public SuperMenu MainMenu { get; set; }
    }
}
