﻿using System;
using System.Collections.Generic;

namespace app.model
{
    public partial class CourseDetails
    {
        public Guid CourseId { get; set; }
        public string CourseName { get; set; }
        public bool? IsActive { get; set; }
        public string CreatedDate { get; set; }
        public Guid? FinancialYearId { get; set; }
        public int? RelivenceCount { get; set; }

        public FinicialYear FinancialYear { get; set; }
    }
}
