﻿using System;
using System.Collections.Generic;

namespace app.model
{
    public partial class FeeReceiptHistory
    {
        public Guid? AdmissionId { get; set; }
        public Guid FeeReceiptId { get; set; }
        public decimal? PaidFee { get; set; }
        public decimal? FeeDue { get; set; }
        public string DueDate { get; set; }
        public short? DueCount { get; set; }
        public string NextDue { get; set; }
        public decimal? FeeAmount { get; set; }
        public int? ReceiptNo { get; set; }
        public bool? Cheque { get; set; }
        public string ChequeNo { get; set; }
        public string Dated { get; set; }
        public string BankName { get; set; }
        public bool? PaymentClear { get; set; }
        public string CreatedDate { get; set; }
    }
}
