﻿using System;
using System.Collections.Generic;

namespace app.model
{
    public partial class ErrorLog
    {
        public int ErrorId { get; set; }
        public string PageName { get; set; }
        public string MethodName { get; set; }
        public DateTime? DateTime { get; set; }
        public string StackTrace { get; set; }
    }
}
