﻿using System;
using System.Collections.Generic;

namespace app.model
{
    public partial class IssueCashDetails
    {
        public Guid CashId { get; set; }
        public string Name { get; set; }
        public string Date { get; set; }
        public decimal? Amount { get; set; }
        public bool? IsActive { get; set; }
        public Guid FinancialYearId { get; set; }
    }
}
