﻿using System;
using System.Collections.Generic;

namespace app.model
{
    public partial class TrainerPaymentDetails
    {
        public TrainerPaymentDetails()
        {
            TrainerBatchDetails = new HashSet<TrainerBatchDetails>();
        }

        public Guid? EmployeeId { get; set; }
        public Guid TrainerId { get; set; }
        public string CreatedDate { get; set; }
        public bool? IsPaymentClear { get; set; }
        public Guid? FinancialYearId { get; set; }
        public Guid? CourseId { get; set; }
        public decimal? TotalPayment { get; set; }
        public decimal? DuePayment { get; set; }
        public decimal? StudFeeCollected { get; set; }
        public decimal? StudTotalFee { get; set; }
        public decimal? StudDueFee { get; set; }
        public decimal? TrainerPercent { get; set; }
        public int? TotalStudentCount { get; set; }
        public string LastVoucherCreated { get; set; }
        public decimal? PaymentMade { get; set; }

        public EmployeeDetails Employee { get; set; }
        public FinicialYear FinancialYear { get; set; }
        public ICollection<TrainerBatchDetails> TrainerBatchDetails { get; set; }
    }
}
