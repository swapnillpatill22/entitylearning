﻿using System;
using System.Collections.Generic;

namespace app.model
{
    public partial class BranchDetails
    {
        public BranchDetails()
        {
            EmployeeDetails = new HashSet<EmployeeDetails>();
            FinicialYear = new HashSet<FinicialYear>();
        }

        public Guid BranchId { get; set; }
        public string BranchName { get; set; }
        public string CreatedDate { get; set; }
        public bool? IsParentBranch { get; set; }
        public bool? IsActive { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string PinCode { get; set; }
        public string BranchPrefix { get; set; }

        public BranchMailServerSettings BranchMailServerSettings { get; set; }
        public ICollection<EmployeeDetails> EmployeeDetails { get; set; }
        public ICollection<FinicialYear> FinicialYear { get; set; }
    }
}
