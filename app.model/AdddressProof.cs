﻿using System;
using System.Collections.Generic;

namespace app.model
{
    public partial class AdddressProof
    {
        public Guid AddressProofId { get; set; }
        public string Title { get; set; }
    }
}
