﻿using App.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;

namespace App.Data
{
   public  class ConnectionContext: DbContext
    {
        public DbSet<Samurai> Samurais { get; set; }
        public DbSet<Quote> Quotes { get; set; }
        public DbSet<Battle> Battles { get; set; }

        #region entity frame work core logging
        public static readonly LoggerFactory MyConsoleLoggerFactory
            = new LoggerFactory(new[] { new ConsoleLoggerProvider((category,level)=>category ==DbLoggerCategory.Database.Command.Name
            && level ==LogLevel.Information,true
            )});
        #endregion

        #region hardcoded connection string in connection context

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseLoggerFactory(MyConsoleLoggerFactory)
                .UseSqlServer(
                 "Server = .\\SQLEXPRESS; Database = SamuraiAppData; Trusted_Connection = True; ");
        }
        #endregion

        //// Constructor is using dependency injection check startup file ConfigureServices method that will set the connection string.
        //public ConnectionContext (DbContextOptions<ConnectionContext> options):base(options)
        //{

        //}


        protected override void OnModelCreating(ModelBuilder modelBuilder)

        {
            //THIS  is used to map many to many relation ship
            modelBuilder.Entity<SamuraiBattle>()
                .HasKey(s => new { s.SamuraiId, s.BattleId });


            base.OnModelCreating(modelBuilder);
        }
    }
}
