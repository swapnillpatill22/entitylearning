﻿using System;
using App.Data;
using App.Domain;
using Microsoft.EntityFrameworkCore;
using System.Linq;
namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //AddSingleObj();
            // AddMultipleObjSameType();
            //AddMultipleObjDifferentType();
            //getAllData();
            // getFilteredQuery();
            //SingleUpdate();
            // batchUpdate();
            //multidboperations();
            //SingleUpdateDisconnected();
            Console.ReadLine();
        }


        #region Add methods
        static void AddSingleObj()
        {
            var saurie = new Samurai { Name = "Swapnil" };
            using (var context = new ConnectionContext())
            {
                context.Samurais.Add(saurie);
                context.Add(saurie);
                context.SaveChanges();
            }
        }

        static void AddMultipleObjSameType()
        {
            var saurie = new Samurai { Name = "Swapnil" };
            var saurie1 = new Samurai { Name = "rajesh" };
            using (var context = new ConnectionContext())
            {

                context.Samurais.AddRange(saurie, saurie1);
                context.SaveChanges();
            }
        }

        static void AddMultipleObjDifferentType()
        {
            var saurie = new Samurai { Name = "Swapnil" };
            var battle = new Battle { Name = "WW1", StartDate = new DateTime(), EndDate = new DateTime() };
            using (var context = new ConnectionContext())
            {
                // here specific class type is not used.
                context.AddRange(saurie, battle);
                context.SaveChanges();
            }
        }
        #endregion

        #region get methods


        static void getAllData()
        {           
            using (var context = new ConnectionContext())
            {

                var sumeriaList =context.Samurais.ToList();
                
            }
        }

        static void getFilteredQuery()
        {
            using (var context = new ConnectionContext())
            {
                var name = "swapnil";
                var sumeriaList = context.Samurais.Where(s=>s.Name==name).ToList();

            }
        }
        #endregion

        #region update methods
        static void SingleUpdate()
        {
            using (var context = new ConnectionContext())
            {
                var name = "swapnil";
                var sumeriaList = context.Samurais.Where(s => s.Name == name).FirstOrDefault();
                sumeriaList.Name = "ranjeet";
                context.SaveChanges();
            }
        }

        static void batchUpdate()
        {//update multiple rows retured
            using (var context = new ConnectionContext())
            {
                var name = "swapnil";
                var sumeriaList = context.Samurais.Where(s => s.Name == name).ToList();
                sumeriaList.ForEach(s => s.Name = "swapnil patil");
                context.SaveChanges();
            }
        }
        #endregion

        #region Multiple db operations

        static void multidboperations()
        {
            var saurie = new Samurai { Name = "test object" };
            using (var context = new ConnectionContext())
            {
                context.Samurais.Add(saurie);
                context.Add(saurie);
                var name = "Swapnil";
                var sumeriaList = context.Samurais.Where(s => s.Name == name).FirstOrDefault();
                sumeriaList.Name = "ranjeet";
                context.SaveChanges();
            }


        }

        #endregion



        /*
         in case of webapi all the context are not aware with other contexts.
         */
        #region update methods disconnected environment
        static void SingleUpdateDisconnected()
        {
            //consider this part of code is received into web api put method 
            //here i have simulated it 
            var sumeriaList= new Samurai();
            using (var context = new ConnectionContext())
            {
                var name = "swapnil";
                sumeriaList = context.Samurais.Where(s => s.Name == name).FirstOrDefault();
                sumeriaList.Name = "ranjeet";

            }
            // this is way to update the received object.
            using (var context = new ConnectionContext())
            {
                context.Samurais.Update(sumeriaList);
                context.SaveChanges();


            }
            //note this method will update all the values 

        }

        static void batchUpdateDisconnected()
        {//update multiple rows retured
            using (var context = new ConnectionContext())
            {
                var name = "swapnil";
                var sumeriaList = context.Samurais.Where(s => s.Name == name).ToList();
                sumeriaList.ForEach(s => s.Name = "swapnil patil");
                context.SaveChanges();
            }
        }
        #endregion
    }
}
